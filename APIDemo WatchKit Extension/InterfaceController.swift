//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    var gameList:[Game] = []
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print(message)
        print("------")
        print(message["gameList"])
        //gameList.append(Game(gameSection: "Quarter-final", gameDate: "27 Jun 2019", gameTime: "21:00", location: "Stade Oceane - Le Havre", coordinates: CLLocationCoordinate2DMake(43.6532, -79.3832), teamA: "Norway", teamB: "England"))
        
        
        
        var gamesList2 = message["gameList"] as! String
  
        print("%%%%%%%%%")
        
        //let json = "{ \"people\": [{ \"firstName\": \"Paul\", \"lastName\": \"Hudson\", \"isAlive\": true }, { \"firstName\": \"Angela\", \"lastName\": \"Merkel\", \"isAlive\": true }, { \"firstName\": \"George\", \"lastName\": \"Washington\", \"isAlive\": false } ] }"
//
        if let data = gamesList2.data(using: .utf8) {
            if let json = try? JSON(data: data) {
                print(json)
                for item in json.arrayValue {
                    print(item.dictionary)
                    
                    var game = item.dictionary
                    self.gameList.append(Game(gameSection: game!["gameSection"]!.string!, gameDate: game!["Date"]!.string!, gameTime: game!["Time"]!.string!, location: game!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: game!["TeamA"]!.string!, teamB: game!["TeamB"]!.string!))
                    
                }
            }
        }
        
//        do {
//            var string="abcd"
//            var encodedString : NSData = (string as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
//            var finalJSON = try JSON(data: encodedString as Data)
//        }
//        catch {
//            print("ERROR!")
//        }
        
        
        
        
        print("DONE!")
        /*
        var jsonResponse = JSON(message).dictionary
        print(jsonResponse)
        print("got the message")
        print("---------")
        
        
        print("[\(message["gameList"]!)]")
        
        var test30 = JSON("[\(message["gameList"]!)]").array
        
        print("$$$$$$$$$$$$$$$$$$$$$$$$")
        
        var test = "\(message["gameList"]!)"
        var test2 = test.components(separatedBy: ", ");
        print("-----test2----")
        print(test2)
        var test3 = JSON(test2).array
        print("----test3----")
        print(test3)
        var test4 = JSON(test3![0]).string
        print("----test4----")
        print(test4)
        var test5 = JSON(test4)
        print("----test5----")
        print(test5)
        */
        //var test = "[" + "\(message["gameList"]!)" + "]"
        //let newString = test.replacingOccurrences(of: "\"", with: "\\\"", options: .literal, range: nil)
        //var tes2 = JSON(newString)
        
        //self.jsonResponse = JSON(apiData)
        
        //let sunriseTime = jsonResponse["results"]["sunrise"].string
        
        //print("\(semifinal!["gameDate"]!.string!)")
        
        /*
        var semifinal = tes2["gameList"][0].dictionary
        print(semifinal)
        print("-------gagsasgagagsgagasg--")
        print(tes2[0])
        //print(tes2[0].dictionaryObject!)
        print("-------gagsasgagagsgagasg--")
        //print(tes2[0]["gameSection"].string!)
        var test3 = message["gameList"]!
        print("\(test3[0][""])")
        */
        /*
        for (index, e) in jsonResponse!.enumerated() {
            
            print(e)
            print("---------")
        }*/
        
        /*
        message.replacingOccurrences(of: "\\", with: "")
        var jsonResponse = JSON(message)
        //jsonResponse = jsonResponse.replace(/\\/g, "");
        print(jsonResponse)*/
        /*
        for item in message {
        gameList.append(Game(gameSection: item["gameSection"], gameDate: item["gameDate"], gameTime: item["gameTime"], location: item["location"], coordinates: CLLocationCoordinate2D(latitude: item["lat"], longitude: item["lon"]), teamA: item["teamA"], teamB: item["teamB"]))
        }*/
    }
    
    // MARK: Outlet
    // ---------------
 
    
    // MARK: Actions
    @IBAction func getDataPressed() {
        print("Watch button pressed")
       
    }
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported()) {
            print("WATCH: WCSession is supported!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WATCH: Does not support WCSession, sorry!")
        }
        
    }
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let n1 = segue.destination as! GamesTableViewController
        n1.gameList = self.gameList
        n1.jsonResponse = self.jsonResponse
        
    }*/

}
