//
//  Home.swift
//  APIDemo
//
//  Created by MacStudent on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FirebaseFirestore
//import CoreLocation
import MapKit


class HomeViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func gamesListBtn(_ sender: UIButton) {
        
    }
    var annotation:MKAnnotation!
    var manager:CLLocationManager!
    var db:Firestore!
    var gameList:[Game] = []
    var jsonResponse: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        
        
      let URL = "https://fifa-71534.firebaseio.com/matches.json"
        //
        Alamofire.request(URL).responseJSON {
            response in
            
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            
            self.jsonResponse = JSON(apiData)
            
            
            
            
             var semifinal = self.jsonResponse["Semi-final"][0].dictionary
           self.gameList.append(Game(gameSection: "Semi-final", gameDate: semifinal!["Date"]!.string!, gameTime: semifinal!["Time"]!.string!, location: semifinal!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: semifinal!["TeamA"]!.string!, teamB: semifinal!["TeamB"]!.string!))
            var semifinal2 = self.jsonResponse["Semi-final"][1].dictionary
            self.gameList.append(Game(gameSection: "Semi-final", gameDate: semifinal2!["Date"]!.string!, gameTime: semifinal2!["Time"]!.string!, location: semifinal2!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: semifinal2!["TeamA"]!.string!, teamB: semifinal2!["TeamB"]!.string!))
           var final = self.jsonResponse["Final"][0].dictionary
           self.gameList.append(Game(gameSection: "Final", gameDate: final!["Date"]!.string!, gameTime: final!["Time"]!.string!, location: final!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: final!["TeamA"]!.string!, teamB: final!["TeamB"]!.string!))
//            var final = self.jsonResponse["Final"][0].dictionary
//            self.gameList.append(Game(gameSection: "Final", gameDate: final!["Date"]!.string!, gameTime: final!["Time"]!.string!, location: final!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: final!["TeamA"]!.string!, teamB: final!["TeamB"]!.string!))

            var quarterFl = self.jsonResponse["Quarter-final"][0].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: quarterFl!["Date"]!.string!, gameTime: quarterFl!["Time"]!.string!, location: quarterFl!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: quarterFl!["TeamA"]!.string!, teamB: quarterFl!["TeamB"]!.string!))
            var quarterF2 = self.jsonResponse["Quarter-final"][1].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: quarterF2!["Date"]!.string!, gameTime: quarterF2!["Time"]!.string!, location: quarterF2!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.383), teamA: quarterF2!["TeamA"]!.string!, teamB: quarterF2!["TeamB"]!.string!))
            var quarterF3 = self.jsonResponse["Quarter-final"][2].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: quarterF3!["Date"]!.string!, gameTime: quarterF3!["Time"]!.string!, location: quarterF3!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.663, longitude: -79.283), teamA: quarterF3!["TeamA"]!.string!, teamB: quarterF3!["TeamB"]!.string!))
            var quarterF4 = self.jsonResponse["Quarter-final"][3].dictionary
            self.gameList.append(Game(gameSection: "Quarter-final", gameDate: quarterF4!["Date"]!.string!, gameTime: quarterF4!["Time"]!.string!, location: quarterF4!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.613, longitude: -79.483), teamA: quarterF4!["TeamA"]!.string!, teamB: quarterF4!["TeamB"]!.string!))

            
            
            var annotations = [MKPointAnnotation]()
            for (_, game) in self.gameList.enumerated() {
                let annotation = MKPointAnnotation()
                annotation.coordinate = game.Coordinates!
                annotation.title = "\(game.TeamA!) vs \(game.TeamB!)"
                annotations.append(annotation)
            }
            self.mapView.addAnnotations(annotations)
            
            let x = CLLocationCoordinate2DMake(43.6532, -79.3832)
            let y = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let z = MKCoordinateRegion(center: x, span: y)
            self.mapView.setRegion(z, animated: true)
            
            self.reorderGamelist()
        }
    }
    
    func reorderGamelist(){
        
        self.gameList = gameList.sorted(by: {
            let splitString = $0.GameDate!.components(separatedBy: " ");
            let splitString2 = $1.GameDate!.components(separatedBy: " ");
            var m0 = 0
            var m1 = 0
            if(splitString[1] == "Jun"){m0 = 6}else{m0 = 7}
            if(splitString2[1] == "Jun"){m1 = 6}else{m1 = 7}
            
            
            
            let date0 = "" + splitString[2] + "\(m0)" + splitString[0] + $0.GameTime
            let date1 = "" + splitString2[2] + "\(m1)" + splitString2[0] + $1.GameTime
            
            return (date0 < date1)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let n1 = segue.destination as! GamesTableViewController
        n1.gameList = self.gameList
        n1.jsonResponse = self.jsonResponse
        
    }
    
    
}
